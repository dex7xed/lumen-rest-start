<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {

    $router->group(['prefix' => 'user', 'as' => 'user'], function () use ($router) {
        /* Default*/
        $router->post('/register',  [
            'as' => 'register',
            'uses' => 'AuthController@register'
        ]);
        $router->post('/sign-in',  [
            'as' => 'login',
            'uses' => 'AuthController@login'
        ]);
        /* Password Recover */
        $router->post('/recover-password',  [
            'as' => 'password.forgot',
            'uses' => 'AuthController@forgotPassword'
        ]);
        $router->patch('/recover-password/{token}',  [
            'as' => 'password.recover',
            'uses' => 'AuthController@recoverPassword'
        ]);
        /* Companies */
        $router->get('/companies',  [
            'as' => 'companies.get',
            'uses' => 'CompanyController@getCompanies',
            'middleware' => 'auth'
        ]);
        $router->post('/companies',  [
            'as' => 'companies.set',
            'uses' => 'CompanyController@setCompanies',
            'middleware' => 'auth'
        ]);

    });

});
