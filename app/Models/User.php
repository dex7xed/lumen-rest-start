<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * @param $first_name
     * @param $last_name
     * @param $phone
     * @param $email
     * @param $password
     * @return false|static
     */
    public static function createFromValues($first_name, $last_name, $phone, $email, $password)
    {
        $user = new static;

        $user->first_name = $first_name;
        $user->last_name = $last_name;
        $user->phone = $phone;
        $user->email = $email;
        $user->password = Hash::make($password);
        $user->verification_token = Str::random(64);

        return $user->save() ? $user : false;
    }

    /**
     * @param $email
     * @return mixed
     */
    public static function byEmail($email)
    {
        return (new static)->where(compact('email'))->first();
    }

    /**
     * @return false|string
     */
    public function createPasswordRecoveryToken()
    {
        $token = Str::random(64);

        $created = DB::table('password_resets')->updateOrInsert(
            ['email' => $this->email],
            ['email' => $this->email, 'token' => $token]
        );

        return $created ? $token : false;
    }

    /**
     * @param $token
     * @param $password
     * @return false
     */
    public static function newPasswordByResetToken($token, $password): bool
    {
        $query = DB::table('password_resets')->where(compact('token'));
        $record = $query->first();

        if (!$record) {
            return false;
        }

        $user = self::byEmail($record->email);

        $query->delete();

        return $user->setPassword($password);
    }

    /**
     * @param $password
     * @return bool
     */
    public function setPassword($password): bool
    {
        $this->password = Hash::make($password);
        return $this->save();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function company(): \Illuminate\Database\Eloquent\Relations\BelongsToMany
    {
        return $this->belongsToMany(Company::class, 'user_companies');
    }

}
