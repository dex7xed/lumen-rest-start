<?php


namespace App\Http\Services;


use App\Models\Company;
use mysql_xdevapi\Exception;

class CompanyService
{
    public function getCompaniesByUserId($userId)
    {
        $companies = Company::with('users')->where('user_id', $userId)->get();
        return $companies ? $companies : response()->json(['data' => ['message' => 'Companies not found for user.']]);
    }

    public function setCompanyByUserId($request, $userId): \Illuminate\Http\JsonResponse
    {
        try {
            $company = Company::create($request);
            $company->users()->attach($userId);

            return response()->json(['data' => ['message' => 'Company created.']]);
        } catch (Exception $e){
            //log
            return response()->json(['data' => ['message' => 'Company not created.']]);
        }

    }
}
